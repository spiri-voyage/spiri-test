//
//  ContentView.swift
//  spiritest
//
//  Created by Rony on 5/31/21.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var model = Model()
    
    
    var body: some View {
        if model.playbackState == PlaybackState.Stopped {
            VStack {
                Picker("Category", selection: $model.selectedCategory){
                    ForEach(model.categories) { category in
                        Text(category.name).tag(category.id)
                    }
                }.padding()
                Picker("Mode", selection: $model.selectedMode){
                    ForEach(model.modes) { mode in
                        Text(mode.name).tag(mode.id)
                    }
                }.padding()
            }
            .pickerStyle(SegmentedPickerStyle())
            
            NavigationView {
                List(selection: $model.selectedOptions) {
                    ForEach(model.options) { option in
                        Text(option.name).tag(option.id)
                    }
                }
                .toolbar { EditButton() }
            }.padding().frame(height: 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
        }

        Button(action: play) {
            Text("Play")
                .font(.title)
        }.padding()

        
        if model.playbackState != PlaybackState.Stopped {
            
            Button(action: pause) {
                Text("Pause")
                    .font(.title)
            }.padding()
            
            Button(action: stop) {
                Text("Stop")
                    .font(.title)
            }.padding()
            
            Button(action: clear) {
                Text("Clear")
                    .font(.title)
            }.padding()
            
            Divider()
                .padding(.vertical, 10.0)
        }
        
        ScrollView {
            Text(model.info)
        }
    }
    
    func play() {
        model.play()
    }
    
    func pause() {
        model.pause()
    }
    
    func stop() {
        model.stop()
    }
    
    func clear() {
        model.clear()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
