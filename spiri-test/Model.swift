//
//  LocationManager.swift
//  spiritest
//
//  Created by Rony on 5/31/21.
//

import Foundation
import AVFoundation
import MediaPlayer

enum PlaybackState {
    case Playing, Paused, Stopped
}

struct Category: Identifiable, Hashable {
    let name: String
    let id: AVAudioSession.Category
}

struct Mode: Identifiable, Hashable {
    let name: String
    let id: AVAudioSession.Mode
}

struct Option: Identifiable, Hashable {
    let name: String
    let id: Int
}

class Model: NSObject, ObservableObject {
    private var appInBackground = false
    private var player = AVPlayer()
    public var categories = [Category(name: "soloAmbient", id: .soloAmbient), Category(name: "ambient", id: .ambient), Category(name: "playback", id: .playback)]
    public var modes = [Mode(name: "default", id: .default), Mode(name: "spokenAudio", id: .spokenAudio)]
    public var options = [Option(name: "duckOthers", id: 0), Option(name: "interruptSpokenAudioAndMixWithOthers", id: 1), Option(name: "allowBluetoothA2DP", id: 2), Option(name: "allowAirPlay", id: 3)]
    @Published var playbackState: PlaybackState = .Stopped
    @Published var info = ""
    @Published var selectedCategory = AVAudioSession.Category.playback
    @Published var selectedMode = AVAudioSession.Mode.spokenAudio
    @Published var selectedOptions = Set<Int>()
    
    override init() {
        super.init()
        selectedOptions.insert(0)
        selectedOptions.insert(1)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didEnterBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(willEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(finishedPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil)
    }
    
    func play() {
        if playbackState == .Stopped {
            let url = Bundle.main.path(forResource: "Kolinbrunnen", ofType: "mp3")
            let item = AVPlayerItem(url: URL(fileURLWithPath: url!))
            player.replaceCurrentItem(with: item)
            let audioSession = AVAudioSession.sharedInstance()
            
            do {
                try audioSession.setCategory(selectedCategory, mode: selectedMode, options: mapOptions())
                addInfo("Audio session category configured for foreground.")
                try audioSession.setActive(true)
                addInfo("Audio session activated.")
                player.play()
                addInfo(playText())
                playbackState = .Playing
            }
            catch {
                addInfo("Failed to play audio: \(error).")
            }
        }
        else if playbackState == .Paused {
            player.play()
            addInfo("Audio resumed.")
            playbackState = .Playing
        }
        else {
            addInfo("Ignoring play command: audio already playing.")
        }
    }
    
    func stop() {
        player.pause()
        addInfo("Audio paused.")
        player.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
        player.replaceCurrentItem(with: nil)
        deactivateAudioSession()
        playbackState = .Stopped
    }
    
    func pause() {
        if playbackState != .Stopped {
            player.pause()
            addInfo("Audio paused.")
            playbackState = .Paused
        }
        else {
            addInfo("Ignoring pause command: audio already stopped.")
        }
    }
    
    func clear() {
        info = ""
    }
    
    private func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false, options: [.notifyOthersOnDeactivation])
            addInfo("Audio session deactivated.")
        }
        catch {
            addInfo("Failed to deactivate audio session.")
        }
    }
    
    private func addInfo(_ text: String) {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "dd.MM.yyyy HH:mm:ss"
        let timestamp = format.string(from: date)
        info = timestamp + ", " + text + "\n" + info
    }
    
    private func playText() -> String {
        return "Playing audio: category = " + selectedCategory.rawValue + ", mode = " + selectedMode.rawValue + ", options = " + mapOptionsDisplay()
    }
    
    @objc private func finishedPlaying(notification: Notification) {
        addInfo("Finished playing audio.")
        deactivateAudioSession()
        playbackState = .Stopped
    }
    
    @objc private func didEnterBackground(_ notification: Notification) {
        addInfo("App entered background.")
        appInBackground = true
        
        if playbackState == .Stopped {
            let url = Bundle.main.path(forResource: "Kolinbrunnen", ofType: "mp3")
            let item = AVPlayerItem(url: URL(fileURLWithPath: url!))
            player.replaceCurrentItem(with: item)
            let audioSession = AVAudioSession.sharedInstance()
            
            do {
                try audioSession.setCategory(selectedCategory, mode: selectedMode, options: mapOptions())
                addInfo("Audio session category configured for background.")
                
                try audioSession.setActive(true)
                addInfo("Audio session activated.")
                player.play()
                addInfo(playText())
                playbackState = .Playing
            }
            catch {
                addInfo("Failed to play audio: error setting category or activating audio session.")
            }
        }
        else {
            addInfo("Ignoring play command: audio not stopped.")
        }
    }
    
    @objc private func willEnterForeground(_ notification: Notification) {
        addInfo("App entered foreground.")
        appInBackground = false
        
//        if playbackState != .Stopped {
//            do {
//                try AVAudioSession.sharedInstance().setCategory(selectedCategory, mode: selectedMode, options: mapOptions())
//                addInfo("Audio session category configured for foreground.")
//            }
//            catch {
//                addInfo("Failed to set audio category.")
//            }
//        }
    }
    
    private func mapOptions() -> AVAudioSession.CategoryOptions {
        var result: AVAudioSession.CategoryOptions = []
        
        for option in selectedOptions {
            if option == 0 {
                result.insert(AVAudioSession.CategoryOptions.duckOthers)
            }
            else if option == 1 {
                result.insert(AVAudioSession.CategoryOptions.interruptSpokenAudioAndMixWithOthers)
            }
            else if option == 2 {
                result.insert(AVAudioSession.CategoryOptions.allowBluetoothA2DP)
            }
            else if option == 3 {
                result.insert(AVAudioSession.CategoryOptions.allowAirPlay)
            }
        }
        
        return result
    }
    
    private func mapOptionsDisplay() -> String {
        var result = ""
        
        for option in selectedOptions {
            if option == 0 {
                result += "duckOthers, "
            }
            else if option == 1 {
                result += "interruptSpokenAudioAndMixWithOthers, "
            }
            else if option == 2 {
                result += "allowBluetoothA2DP, "
            }
            else if option == 3 {
                result += "allowAirPlay, "
            }
        }
        
        return result
    }
    
}
